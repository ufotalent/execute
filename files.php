<?php
	$chosen='Files';
	include ("side.php");
?>
		
		<div id="main-content"> <!-- Main Content Section with everything -->
			
			<noscript> <!-- Show a notification if the user has disabled javascript -->
				<div class="notification error png_bg">
					<div>
						Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
					Download From <a href="http://www.exet.tk">exet.tk</a></div>
				</div>
			</noscript>
			
			<!-- Page Head -->
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>Command Line</h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">Current</a></li> <!-- href must be unique and match the id of target div -->
						<li><a href="#tab2">History</a></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

						<form action="#" method="post">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
						<pre>
<?php

	$fp=fopen("log","r");
	$num=0;
	while ($buf=fgets($fp)) {
		if ($buf==""|| $buf=="\r" || $buf=="\n")
			continue;
		$num++;
		$data[$num]=$buf;
	}
	fclose($fp);
	if (isset($_REQUEST["cmd"])) {
		system("rm result");
		$cmd=$_REQUEST["cmd"];
		if ($cmd=="clear") {
			$startprint=$num+1;
		} else {
			$result=system("$cmd > result",$ret);
			if ($ret!=0) {
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>Execution failed</div></div>';
			}
			$num++;
			$data[$num]=">$cmd";
			$fp=fopen("result","r");
			while ($buf=fgets($fp)) {
				$num++;
				$data[$num]=$buf;
			}
			fclose($fp);
		}
	} 
//	$_SESSION["startprint"]=0;
	$fp=fopen("log","w");
	$startprint=$_SESSION["startprint"];
	if ($num-35>$startprint) {
		$startprint=$num-35;
		$_SESSION["startprint"]=$startprint;
	}
	for ($i=1;$i<=$num;$i++) {
		if ($i>$startprint) {
			echo htmlspecialchars($data[$i]);
			$now=$data[$i];
			$now[strlen($now)-1];
			if ($now[strlen($now)-1]!="\n" && $now[strlen($now)-1]!="\r")
				echo '<br>';
		}
		fputs($fp,"$data[$i]\n");
	}
	fclose($fp);

?></pre>
								
								
								<p>
									<label>Your Command</label>
									<input class="text-input large-input" type="text" id="large-input" name="cmd" />
								</p>
								
								
								<p>
									<input class="button" type="submit" value="Submit" />
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>

					</div> <!-- End #tab1 -->
					
					<div class="tab-content" id="tab2">
					
						<form action="#" method="post">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
<pre>
<?php
	if ($_REQUEST["dropdown"]=="option1") {
		system("rm log");
		$_SESSION["startprint"]=0;
	}
	$fp=fopen("log","r");
	$num=0;
	while ($buf=fgets($fp)) {
		if ($buf==""|| $buf=="\r" || $buf=="\n")
			continue;
		echo htmlspecialchars($buf);
	}

?>
</pre>
								
								
								<p>
									<label>Select Action</label>              
									<select name="dropdown" class="small-input">
										<option value="option1">Clear</option>
									</select> 
								</p>
								
								
								<p>
									<input class="button" type="submit" value="Submit" />
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div> <!-- End #tab2 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			
			
			<!-- End Notifications -->
			
			<div id="footer">
				<small> <!-- Remove this notice or replace it with whatever you want -->
						&#169; Copyright 2011 Ufotalent | Powered by <a href="http://themeforest.net/item/simpla-admin-flexible-user-friendly-admin-skin/46073">Simpla Admin</a> | <a href="#">Top</a>
				</small>
			</div><!-- End #footer -->
			
		</div> <!-- End #main-content -->
		
	</div></body>
  

<!-- Download From www.exet.tk-->
</html>

